package javaCV通讯;

import javax.swing.JFrame;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameRecorder;
import org.bytedeco.javacv.OpenCVFrameConverter;

public class 收流器2 {
	private static void recordByFrame(FFmpegFrameGrabber grabber, FFmpegFrameRecorder recorder, Boolean status)
			throws Exception, org.bytedeco.javacv.FrameRecorder.Exception {
		OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();// 转换器
		CanvasFrame frame = new CanvasFrame("in", CanvasFrame.getDefaultGamma() / grabber.getGamma());
		try {// 建议在线程中使用该方法
			grabber.start();
			recorder.start();

			// IplImage grabbedImage = converter.convert(grabber.grab());//
			// 抓取一帧视频并将其转换为图像，至于用这个图像用来做什么？加水印，人脸识别等等自行添加
			// CanvasFrame frame = new CanvasFrame("camera",
			// CanvasFrame.getDefaultGamma() / grabber.getGamma());
			// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// frame.setAlwaysOnTop(true);
			// Frame rotatedFrame = converter.convert(grabbedImage);//
			// 不知道为什么这里不做转换就不能推到rtmp
			// long startTime = 0;
			// long videoTS = 0;
			// while (frame.isVisible() && (grabbedImage =
			// converter.convert(grabber.grab())) != null) {
			// rotatedFrame = converter.convert(grabbedImage);
			// frame.showImage(rotatedFrame);
			// if (startTime == 0) {
			// startTime = System.currentTimeMillis();
			// }
			// videoTS = 1000 * (System.currentTimeMillis() - startTime);
			// recorder.setTimestamp(videoTS);
			// recorder.record(rotatedFrame);
			// Thread.sleep(40);
			// }

			Frame rotatedFrame = null;
			while (status && (rotatedFrame = grabber.grabFrame()) != null) {
				IplImage grabbedImage2 = converter.convert(rotatedFrame);
				frame.showImage(rotatedFrame);
				recorder.record(rotatedFrame);
			}
			recorder.stop();
			grabber.stop();
		} finally {
			if (grabber != null) {
				grabber.stop();
			}
		}
	}

	/**
	 * 按帧录制视频
	 * 
	 * @param inputFile-该地址可以是网络直播/录播地址，也可以是远程/本地文件路径
	 * @param outputFile
	 *            -该地址只能是文件地址，如果使用该方法推送流媒体服务器会报错，原因是没有设置编码格式
	 * @throws FrameGrabber.Exception
	 * @throws FrameRecorder.Exception
	 * @throws org.bytedeco.javacv.FrameRecorder.Exception
	 */
	public static void frameRecord(String inputFile, String outputFile, int audioChannel)
			throws Exception, org.bytedeco.javacv.FrameRecorder.Exception {

		boolean isStart = true;// 该变量建议设置为全局控制变量，用于控制录制结束
		// 获取视频源
		FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(inputFile);
		// 流媒体输出地址，分辨率（长，高），是否录制音频（0:不录制/1:录制）
		FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(outputFile, 1280, 720, audioChannel);
		// 开始取视频源
		recordByFrame(grabber, recorder, isStart);
	}

//	public static void main(String[] args)
//			throws FrameRecorder.Exception, FrameGrabber.Exception, InterruptedException, Exception {
//		String path = "C:/Users/823811845/Desktop/";
//		// String inputFile = "output.mp4";
//
//		String inputFile = "FLV测试文件.flv";
//		// Decodes-encodes
//		String outputFile = "recorde.flv";
//		frameRecord(path + inputFile, path + outputFile, 1);
//	}
	
	 public static void main(String[] args)
	 throws FrameRecorder.Exception, FrameGrabber.Exception,
	 InterruptedException, Exception {
	// String inputFile ="rtsp://admin:admin@192.168.2.236:37779/cam/realmonitor?channel=1&subtype=0";
	 String inputFile = "rtmp://localhost/live/";
	 // Decodes-encodes
//	 String path = "C:/Users/823811845/Desktop/";
	 String path ="";
	 String outputFile = "recorde.flv";
	 frameRecord(inputFile, path+outputFile, 0);
	 }
}
